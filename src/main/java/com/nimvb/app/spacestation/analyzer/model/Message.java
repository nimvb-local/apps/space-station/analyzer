package com.nimvb.app.spacestation.analyzer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class Message {
    @JsonProperty("id")
    private String id;
    @JsonProperty("response_timestamp")
    private long timestamp;
    @JsonProperty("data")
    private Payload payload;
}
