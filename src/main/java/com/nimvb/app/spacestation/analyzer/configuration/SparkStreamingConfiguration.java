package com.nimvb.app.spacestation.analyzer.configuration;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SparkStreamingConfiguration {

    @Bean
    JavaStreamingContext javaStreamingContext(JavaSparkContext context){
        return new JavaStreamingContext(context,new Duration(6000));
    }
}
