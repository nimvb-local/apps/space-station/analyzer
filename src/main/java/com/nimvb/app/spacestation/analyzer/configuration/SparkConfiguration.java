package com.nimvb.app.spacestation.analyzer.configuration;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SparkConfiguration {

    @Bean
    SparkConf sparkConf(){
        return new SparkConf().setAppName("analyzer").setMaster("local[*]");
    }

    @Bean
    JavaSparkContext javaSparkContext(SparkConf sparkConf){
        return new JavaSparkContext(sparkConf);
    }

    @Bean
    SparkContext sparkContext(JavaSparkContext javaSparkContext){
        return javaSparkContext.sc();
    }

    @Bean
    SparkSession sparkSession(SparkContext sparkContext){
        return SparkSession.builder().sparkContext(sparkContext).getOrCreate();
    }
}
