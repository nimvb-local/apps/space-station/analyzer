package com.nimvb.app.spacestation.analyzer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.WriteConfig;
import com.nimvb.app.spacestation.analyzer.model.Message;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.*;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class AnalyzerApplication implements CommandLineRunner {

    private final JavaStreamingContext streamingContext;
    private final WriteConfig writeConfig;
    private final OffsetCommitCallback offsetCommitCallback;
    private final Map<String, Object> kafkaConfigMap;

    public AnalyzerApplication(JavaStreamingContext streamingContext, WriteConfig writeConfig, OffsetCommitCallback offsetCommitCallback, @Qualifier("kafkaConfigMap") HashMap<String, Object> kafkaConfigMap) {
        this.streamingContext = streamingContext;
        this.writeConfig = writeConfig;
        this.offsetCommitCallback = offsetCommitCallback;
        this.kafkaConfigMap = kafkaConfigMap;
    }


    public static void main(String[] args) {
        SpringApplication.run(AnalyzerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        final JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils.createDirectStream(streamingContext,
                LocationStrategies.PreferConsistent(),
                ConsumerStrategies.Subscribe(List.of("space-station-location"), kafkaConfigMap));

        final JavaDStream<Document> documents = stream
                .filter(record -> {
                    ObjectMapper mapper = new ObjectMapper();
                    final Message message = mapper.readValue(record.value(), Message.class);
                    return message != null && message.getPayload() != null && message.getPayload().isSuccess();
                })
                .map(record -> Document.parse(record.value()));

        documents.foreachRDD(doc -> {
            MongoSpark.save(doc,writeConfig);
        });

        stream.foreachRDD(rdd -> {
            KafkaRDD<String, String> kafkaRdd = (KafkaRDD<String, String>) rdd.rdd();
            DirectKafkaInputDStream<String,String> kafkaInputDStream = (DirectKafkaInputDStream<String,String>) stream.inputDStream();
            kafkaInputDStream.commitAsync(kafkaRdd.offsetRanges(),offsetCommitCallback);
        });


        streamingContext.start();
        streamingContext.awaitTermination();
    }
}
