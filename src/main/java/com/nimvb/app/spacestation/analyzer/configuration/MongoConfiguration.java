package com.nimvb.app.spacestation.analyzer.configuration;

import com.mongodb.spark.config.WriteConfig;
import org.apache.spark.SparkConf;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MongoConfiguration {

    @Bean
    WriteConfig writeConfig(SparkConf sparkConf){
        return WriteConfig.create(mongoConfigMap());
    }

    private Map<String,String> mongoConfigMap(){
        Map<String,String> config = new HashMap<>();
        config.put("spark.mongodb.output.uri","mongodb://mongodb:27017/");
        config.put("spark.mongodb.output.database","station-db");
        config.put("spark.mongodb.output.collection","location");
        return config;
    }

}
