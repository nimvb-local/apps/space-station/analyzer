package com.nimvb.app.spacestation.analyzer.service;

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class KafkaOffsetCommitCallback implements org.apache.kafka.clients.consumer.OffsetCommitCallback {
    private static final Logger log = LoggerFactory.getLogger(KafkaOffsetCommitCallback.class);
    @Override
    public void onComplete(Map<TopicPartition, OffsetAndMetadata> map, Exception e) {
        log.info("---------------------------");
        log.info("offsets " + map + " committed, exception " + e);
        log.info("---------------------------");

    }
}
